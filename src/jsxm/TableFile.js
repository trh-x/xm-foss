import React from "react";
import Moment from "moment";
import ClassNames from "classnames";

const xmFilesBaseUri = window.xmFilesBaseUri;

function float_precision(float_value, precision) {
  float_value = parseFloat(float_value);
  if (isNaN(float_value)) {
    return parseFloat("0").toFixed(precision);
  } else {
    var power = Math.pow(10, precision);
    float_value = (Math.round(float_value * power) / power).toFixed(precision);
    return float_value.toString();
  }
}

function file_size(size) {
  if (size > 1024) {
    var kb_size = size / 1024;
    if (kb_size > 1024) {
      var mb_size = kb_size / 1024;
      return "" + float_precision(mb_size, 2) + " MB";
    }
    return "" + Math.round(kb_size) + " kB";
  }
  return "" + size + " B";
}

class TableFile extends React.Component {
  constructor(props) {
    super(props);
    this.handleFileClick = this.handleFileClick.bind(this);
    this.handleItemClick = this.handleItemClick.bind(this);
    this.handleItemDoubleClick = this.handleItemDoubleClick.bind(this);
  }

  render() {
    var icon = <i className="fa fa-play-circle" aria-hidden="true"></i>;

    var name = (
      <a
        href={this.props.url || "#"}
        download="download"
        onClick={this.handleFileClick}
      >
        {icon}
        {this.getName()}
      </a>
    );

    // var modified = typeof this.props.modified === 'undefined' ? '-' : Moment(this.props.modified, 'x').fromNow();
    var modified =
      typeof this.props.modified === "undefined"
        ? "-"
        : Moment(this.props.modified, "x").format("LL");

    var row = (
      <tr
        className={ClassNames("file", {
          selected: this.props.isSelected,
        })}
        onClick={this.handleItemClick}
        onDoubleClick={this.handleItemDoubleClick}
      >
        <td className="name">
          <div style={{ paddingLeft: this.props.depth * 16 + "px" }}>
            {name}
            {/* <a href="#" onClick={this.handleFileClick}>
              load
            </a> */}
            <a href={`${xmFilesBaseUri}${this.props.fileKey}`}>download</a>
          </div>
        </td>
        <td className="size">{file_size(this.props.size)}</td>
        <td className="modified">{modified}</td>
      </tr>
    );

    return row;
  }

  // Remaining methods copied from react-keyed-file-browser -> base-file.js
  getName() {
    var name = this.props.newKey || this.props.fileKey;
    var slashIndex = name.lastIndexOf("/");
    if (slashIndex != -1) {
      name = name.substr(slashIndex + 1);
    }
    return name;
  }
  getExtension() {
    var blobs = this.props.fileKey.split(".");
    return blobs[blobs.length - 1].toLowerCase().trim();
  }

  async handleFileClick(event) {
    if (event) {
      event.preventDefault();
    }
    this.props.browserProps.preview({
      url: this.props.url,
      name: this.getName(),
      key: this.props.fileKey,
      extension: this.getExtension(),
    });
    this.props.player.stop();
    try {
      await this.props.player.downloadXM(
        `${xmFilesBaseUri}${this.props.fileKey}`
      );
      this.props.player.play();
    } catch (e) {
      console.warn("Error loading file", e);
    }
  }
  handleItemClick(event) {
    event.stopPropagation();
    this.props.browserProps.select(this.props.fileKey);
  }
  handleItemDoubleClick(event) {
    event.stopPropagation();
    this.handleFileClick();
  }
}

export default TableFile;
