import React from "react";
import PropTypes from "prop-types";
import FileBrowser, { Icons } from "react-keyed-file-browser";
import "font-awesome/css/font-awesome.min.css";

// import TableFile from 'react-keyed-file-browser/src/files/list-thumbnail';
import TableFile from "./TableFile";

import initXMEffects from "./xmeffects";
import initTrackView from "./trackview";
import initXM from "./xm";
import initShell from "./shell";

import "./jsxm.css"; // TODO: CSS Module?

const xmFiles = window.xmFiles;

/* const modified = 1504797846559;

const files = [
    {
        key: 'photos/animals/cat in a hat.png',
        modified,
        size: 1.5 * 1024 * 1024,
    },
    {
        key: 'photos/animals/kitten_ball.png',
        modified,
        size: 545 * 1024,
    },
    {
        key: 'photos/animals/elephants.png',
        modified,
        size: 52 * 1024,
    },
    {
        key: 'photos/funny fall.gif',
        modified,
        size: 13.2 * 1024 * 1024,
    },
    {
        key: 'photos/holiday.jpg',
        modified,
        size: 85 * 1024,
    },
    {
        key: 'documents/letter chunks.doc',
        modified,
        size: 480 * 1024,
    },
    {
        key: 'documents/export.pdf',
        modified,
        size: 4.2 * 1024 * 1024,
    }
]; */

const nullComp = () => null;

// TODO: extends React.PureComponent ?
export default class Player extends React.Component {
  static propTypes = {
    level: PropTypes.number,
    bpmShift: PropTypes.number,
    onLiveChange: PropTypes.func,
  };

  constructor(props) {
    super(props);
    this._player = { onLiveChange: this.props.onLiveChange };
    this._TableFileComp = (props) => (
      <TableFile player={this._player} {...props} />
    );
  }

  componentDidMount() {
    var player = this._player;
    var view = {};

    initXMEffects(player);
    initTrackView(player, view, this._instance);
    initXM(player, view);
    initShell(player, view, this._instance);

    // debugger;
    // this.props.onLiveChange({ bpm: player.xm.bpm });
  }

  componentWillReceiveProps(nextProps) {
    // TODO: Should this be in another lifecycle method? componentDidUpdate() ?
    // TODO: update player's onLiveChange func
    if (nextProps.level !== this.props.level)
      this._player.setLevel(nextProps.level);
    if (nextProps.bpmShift !== this.props.bpmShift)
      this._player.setBpmShift(nextProps.bpmShift);
  }

  _stashRef = (instance) => (this._instance = instance);

  render() {
    return (
      <div
        ref={this._stashRef}
        className="playercontainer"
        onDrop={(evnt) => this._player.handleDrop(evnt)}
        onDragOver={(evnt) => this._player.allowDrop(evnt)}
        onDragLeave={(evnt) => this._player.allowDrop(evnt)}
      >
        <div>
          <canvas className="centered title" width="640" height="22"></canvas>
        </div>
        <div className="hscroll">
          <div>
            <canvas className="centered vu" width="224" height="64"></canvas>
          </div>
          <div>
            <canvas
              className="centered gfxpattern"
              width="640"
              height="504"
            ></canvas>
          </div>
        </div>
        <div className="instruments"></div>
        <div>
          <p style={{ textAlign: "center" }}>
            <button
              className="playpause"
              disabled
              style={{ width: "100px", background: "#ccc" }}
            >
              Play
            </button>
          </p>
        </div>
        <div style={{ display: "none" }} className="filelist"></div>
        <FileBrowser
          files={xmFiles}
          icons={Icons.FontAwesome(4)}
          fileRenderer={this._TableFileComp}
          detailRenderer={nullComp}
        />
      </div>
    );
  }
}
