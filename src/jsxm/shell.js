export default function initShell (player, view, element) {
    var xmFilesBaseUri = window.xmFilesBaseUri;
    var xmFiles = window.xmFiles;

    // FIXME: This is a hack to get the play/pause button working.
    // It should be done using events.
    player.element = element;

    // http://a1k0n-pub.s3-website-us-west-1.amazonaws.com/xm/xmlist.js

    /* var baseuri = "//a1k0n-pub.s3.amazonaws.com/xm/";
    var xmuris = [
        ["Zalza - my dirty old kamel", "kamel.xm"],
        ["Zalza vs. JosSs - Interc00ler", "interc00ler.xm"],
        ["Lizardking - Claustrophobia", "Claustrophobia.xm"],
        ["Lizardking - Transatlantic", "transatlantic.xm"],
        ["Lizardking - Bosvedjan Jam", "bosvedjan.xm"],
        ["xyce - ne pas", "nepas.xm"],
        ["Jugi - Onward", "onward.xm"],
        ["Norfair - Chillzone", "nf-chill.xm"],
        ["Dubmood - LUCiD Keygen chip", 'db_keygen.xm'],
        ["Strobe / TiTaN - Mothership FTW", 'mothershipftw-stroberev2.xm'],
        ["Ghidorah - Toilet story 5", 'Ghidorah_-_Toilet_story_5.xm']
    ]; */


    function loadXMAndInit(xmdata) {
        if (!player.load(xmdata)) {
            return;
        }

        view.init();

        var playbutton = element.querySelector(".playpause");
        playbutton.innerHTML = 'Play';
        playbutton.onclick = function() {
            if (player.playing) {
                player.pause();
                playbutton.innerHTML = 'Play';
            } else {
                player.play();
                playbutton.innerHTML = 'Pause';
            }
        };
        playbutton.disabled = false;

        return player.xm;
    }

    player.downloadXM = function(uri) {
        return new Promise((resolve, reject) => {
            var xmReq = new XMLHttpRequest();
            xmReq.open("GET", uri, true);
            xmReq.responseType = "arraybuffer";
            xmReq.onload = function(xmEvent) {
            var arrayBuffer = xmReq.response;
            if (arrayBuffer) {
                loadXMAndInit(arrayBuffer);
                player.onLiveChange({ bpm: player.xm.bpm });
                resolve(player.xm);
            } else {
                console.log("unable to load", uri);
                reject(new Error("unable to load"));
            }
        };
            xmReq.send(null);
        });
    };

    player.allowDrop = function(ev) {
        ev.stopPropagation();
        ev.preventDefault();
        var elem = element.querySelector(".playercontainer");
        elem.className = (ev.type == "dragover" ? "draghover playercontainer" : "playercontainer");
        return false;
    };

    player.handleDrop = function(e) {
        console.log(e);
        e.preventDefault();
        var elem = element.querySelector(".playercontainer");
        elem.className = "playercontainer";
        var files = e.target.files || e.dataTransfer.files;
        if (files.length < 1) return false;
        var reader = new FileReader();
        reader.onload = function(e) {
            player.stop();
            loadXMAndInit(e.target.result);
        };
        reader.readAsArrayBuffer(files[0]);
        return false;
    };

    /* function initFilelist() {
        var el = element.querySelector('.filelist');
        xmuris.forEach(function(entry) {
            var a = document.createElement('a');
            a.text = entry[0];
            a.href = '#' + entry[1];
            a.onclick = function() {
                el.style.display = "none";
                player.stop();
                player.downloadXM(baseuri + entry[1]);
            };
            el.appendChild(a);
            // TODO: Tidy this up... etc
            a = document.createElement('a');
            a.text = 'leech';
            a.href = baseuri + entry[1];
            a.style.marginLeft = '15px';
            el.appendChild(a);
            el.appendChild(document.createElement('br'));
        });
        var loadbutton = element.querySelector('.loadbutton');
        loadbutton.onclick = function() {
            if (el.style.display == "none") {
                el.style.display = "block";
            } else {
                el.style.display = "none";
            }
        };
    } */

    // TODO: This was wrapped in document onload:
    player.init();
    // initFilelist();

    /* var uri = window.location.hash.substr(1);
    if (uri === "") {
        uri = xmuris[0][1];
    }
    if (!uri.startsWith("http")) {
        uri = baseuri + uri;
    } */
    var defaultXm = xmFiles.sort((a, b) => Math.sign(b.modified - a.modified))[0];
    var uri = xmFilesBaseUri + defaultXm.key;
    player.downloadXM(uri);
}
