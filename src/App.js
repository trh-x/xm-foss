import React from "react";
import logo from "./logo.svg";
import "./App.css";

import Player from "./jsxm/Player";
import Slider from "rc-slider/lib/Slider";
import "rc-slider/assets/index.css";

import throttle from "lodash/throttle";

const SLIDER_THROTTLE_MS = 50;

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      leftBpmShift: 50,
      rightBpmShift: 50,
      crossfade: 50,
      leftPlayer: {
        bpm: 0,
      },
      rightPlayer: {
        bpm: 0,
      },
    };
  }

  _updateCrossfade = throttle((crossfade) => {
    this.setState({ crossfade });
  }, SLIDER_THROTTLE_MS);

  _updateLeftBpmShift = throttle((leftBpmShift) => {
    this.setState({ leftBpmShift });
  }, SLIDER_THROTTLE_MS);

  _updateRightBpmShift = throttle((rightBpmShift) => {
    this.setState({ rightBpmShift });
  }, SLIDER_THROTTLE_MS);

  _leftLevel() {
    // TODO: I'm sure there is a more elegant way to do this, but this will do for now...
    return this.state.crossfade > 50
      ? 100 - (this.state.crossfade - 50) * 2
      : 100;
  }

  _rightLevel() {
    return this.state.crossfade < 50 ? this.state.crossfade * 2 : 100;
  }

  _leftBpmHandle = (props) => {
    console.log("PROPS", props);
    return (
      <div
        role="slider"
        tabIndex="0"
        aria-valuemin={props.min}
        aria-valuemax={props.max}
        aria-valuenow={props.offset}
        aria-disabled="false"
        className="rc-slider-handle"
        style={{ left: `${props.offset}%` }}
      >
        <span className="bpm-shift-value">{this.state.leftPlayer.bpm}</span>
      </div>
    );
  };

  // TODO: DRY
  _rightBpmHandle = (props) => {
    console.log("PROPS", props);
    return (
      <div
        role="slider"
        tabIndex="0"
        aria-valuemin={props.min}
        aria-valuemax={props.max}
        aria-valuenow={props.offset}
        aria-disabled="false"
        className="rc-slider-handle"
        style={{ left: `${props.offset}%` }}
      >
        <span className="bpm-shift-value">{this.state.rightPlayer.bpm}</span>
      </div>
    );
  };

  _updateLeftPlayer = ({ bpm }) => this.setState({ leftPlayer: { bpm } });
  _updateRightPlayer = ({ bpm }) => this.setState({ rightPlayer: { bpm } });

  render() {
    // TODO: Slider should take a range 0 - 200 to ensure we crossfade in steps of 1
    // TODO: Should start using Redux/MobX for things like track BPM etc?
    return (
      <div className="App">
        <div className="App-header">
          <h1>deginge and ThaCount's bits and bops</h1>
          <div className="mix-controls">
            <Slider
              className="pitch-control"
              handle={this._leftBpmHandle}
              value={this.state.leftBpmShift}
              onChange={this._updateLeftBpmShift}
            />
            <Slider
              className="crossfader"
              value={this.state.crossfade}
              onChange={this._updateCrossfade}
            />
            <Slider
              className="pitch-control"
              handle={this._rightBpmHandle}
              value={this.state.rightBpmShift}
              onChange={this._updateRightBpmShift}
            />
          </div>
        </div>
        <div className="App-players">
          <Player
            level={this._leftLevel()}
            bpmShift={this.state.leftBpmShift}
            onLiveChange={this._updateLeftPlayer}
          />
          <Player
            level={this._rightLevel()}
            bpmShift={this.state.rightBpmShift}
            onLiveChange={this._updateRightPlayer}
          />
        </div>
        <pre className="App-footer">
          Bitbucket:{" "}
          <a href="https://bitbucket.org/trh-x/xm-foss/">
            bitbucket.org/trh-x/xm-foss/
          </a>
          <br />
          JSXM code:{" "}
          <a href="http://github.com/a1k0n/jsxm/">github.com/a1k0n/jsxm</a>
          <br />
          Note, some XM FX not yet implemented
          <br />
          <br />
          Shout out to Rob B for getting me to put this back online!
        </pre>
      </div>
    );
  }
}

export default App;
