#!/usr/bin/env node

// !% public/xm 'http://localhost:3000/xm/' > public/choonz-browse.js

const find = require("find");
const path = require("path");
const fs = require("fs");

const dir = process.argv[2];
const baseuri = process.argv[3] || "";

const resolvedDir = path.resolve(dir);
const dirname = path.dirname(resolvedDir);
const basename = path.basename(resolvedDir);

const files = find.fileSync(/\.xm$/i, resolvedDir);

const fileObjs = files.map((filePath) => {
  const stats = fs.statSync(filePath);
  return {
    key: filePath.replace(path.join(dirname, basename) + path.sep, ""),
    modified: stats.mtime.getTime(),
    size: stats.size,
  };
});

console.log(`
xmFilesBaseUri = '${baseuri}';
xmFiles = ${JSON.stringify(fileObjs)};
`);
